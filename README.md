# Smile

<h2> Objectif : </h2>
<p> L'objectif de ce projet est de permettre à des personnes ne pouvant pas sourire suite à des complication médical, de sourire. Et cela en temps réel. </p>

<h2> Installation : </h2>
<p> Pour utiliser l'application, il est nécessaire d'avoir les paquets suivant installer : </p>
<ul>
    <li><strong>python3 : </strong> <code>sudo apt-get install python3</code></li>
    <li><strong>anaconda : </strong>
        <ul>
            <li><a href="https://www.anaconda.com/distribution/">anaconda</a> version python 3.7</li>
            <li><code>conda create -n p37 python=3.7</code></li>
            <li><code>activate p37</code></li>
            <li>Pour retirer le <code>(base)</code> : <code>conda config --set auto_activate_base false</code></li>
        </ul>
    </li>
    <li>Les librairie python suivante :
        <ul>
            <li><strong>Pytorch : </strong> <a href="https://pytorch.org/">pytorch</a></li>
            <li><strong>opencv : </strong><code>conda install -c conda-forge opencv</code></li>
            <li><strong>numpy : </strong><code>conda install -c anaconda numpy</code></li>
            <li><strong>TensorFlow : </strong><code>pip3 install --ignore-installed --upgrade tensorflow-gpun</code></li>
		    <li><strong>Dlib : </strong> 
		        <ul>
		            <li><code>conda install -c menpo dlib or</code></li>
		            <li><code>pip3 install cmake</code></li>
		            <li><code>pip3 install dlib</code></li>
		        </ul>
		    </li>
            <li><strong>skimage : </strong><code>conda install -c anaconda scikit-learn scikit-image</code></li>
            <li><strong>imutils : </strong><code>pip3 install imutils</code></li>
            <li><strong>PIL : </strong><code>conda install -c anaconda pillow</code></li>
            <li><strong>tkinter : </strong><code>sudo apt-get install python3-tk</code></li>
        </ul>
   	</li>
</ul>

<h2> Utilisation : </h2>
<p> Pour lancer l'application lancer la commande : <br>
    <code>python3 Smile.py</code> <br>
    Nécessité de se trouver dans le dossier racine de l'application.
</p> <br>
<p> Plusieurs arguments sont possible lors du lancement de l'application : <br>
    <ul>
        <li><code>-W x</code> : Définit à x la largeur de l'image (480 par default)</li>
        <li><code>-H y</code> : Définit à y la hauteur de l'image (320 par default)</li>
        <li><code>-d cpu</code> : Définit la matériel où les calculs se feront : cpu ou cuda (cpu par default)</li>
        <li><code>--debug</code> : Affiche des éléments de debug pendant l'éxécution du programme</li>
    </ul>
</p> <br>
![image](https://forge.univ-lyon1.fr/p1707053/smile/-/raw/master/Fonctionnement.png)

<h2> Description Code : </h2>
<h3> Technologies utilisées : </h3>
<p> Ce projet utilise utilise principalement 3 technologies :</p>
<ul>
    <li><strong>Python : </strong> utilisé pour l'interface, la gestion vidéo et la cordination avec les deux autres technologies</li>
    <li><strong>Deep Learning : </strong> utilisation de système de neuronne pour détecter les éléments du visage et la modification des points à effectuer pour faire sourir</li>
    <li><strong>Warping : </strong> en prenant des points de départ et des points d'arrivé permet de modifier rapidement une image </li>
</ul>
<h3> Structure du projet : </h3>
<p> La racine du projet contient un fichiers ("Smile.py" : le coeur du projet le fichier a exécuter) et ce fichier. </p>
<p> La racine du projet contient aussi 3 dossiers : </p>
<ul>
    <li><strong>src : </strong> dossier contenant les classes suivante : 
        <ul>
            <li><strong>Interface : </strong> classe qui gère l'interface graphique</li>
            <li><strong>Warping : </strong> classe qui gère la modification de l'image par warping</li>
            <li><strong>OrientationVisage : </strong> classe qui gère la bonne orientation du visage</li>
        </ul>
    </li>
    <li><strong>training : </strong> dossier contenant : 
        <ul>
            <li>les fichiers sources des réseaux de neuronne (Net.py) et l'exécution des entrainement (Training.py)</li>
            <li>Les sauvegardes des différents système de neuronne (contenu dans le dossier Net)</li>
            <li>Les fichiers pour l'entrainement : data.zip en compresser et le fichier data si le zip est décompressé</li>
        </ul>
    </li>
    <li><strong>screen : </strong> dossier pas présent lors du téléchargement, est crée lorsqu'une capture est prise, il contient ces dernières </li>
</ul>