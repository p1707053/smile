from imutils import face_utils

import numpy as np
import pandas as pd

from PIL import Image

import math
import torch
import torch.nn as nn
import torch.optim as optim

import random

import dlib
import cv2

import os as os
import os.path as osp

import Net


device = torch.device("cpu")


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('./Net/face_detector.dat')

rdm = random.Random()

def TraitementImage(frm, w = 160, h =260, tf = Net.transf):

    gray = cv2.cvtColor(frm, cv2.COLOR_BGR2GRAY)

    rects = detector(gray, 0)
    # Pour chaque tete détecté
    for rect in rects:
        # Aggrandissement du rectangle + dessin
        if(rect.left()-20 > 0) :
            left = rect.left()-20
        else :
            left = 0
        if(rect.top()-20 > 0) :
            top = rect.top()-20
        else:
            top = 0

        right = rect.right()+20
        bottom = rect.bottom()+20
        img = frm[top:bottom, left:right]
        img = Image.fromarray(img)
        img = img.resize((w,h))
        return tf(img).to(device)


# Récupération des pt du visage
def PtsVisage(frm):

    gray = cv2.cvtColor(frm, cv2.COLOR_BGR2GRAY)

    rects = detector(gray, 0)
    # Pour chaque tete détecté
    for rect in rects:
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        shape = shape
        shape = shape.astype(int)
        shape = torch.from_numpy(shape)
        return shape

def TrainingNet(netSmile, netPosPts):
    print("Start training")
    imgFolder = './data/Entrainement_Points/'

    netSmile.train()
    netPosPts.train()

    optiNetSmile = optim.SGD(netSmile.parameters(), lr=0.001)
    critNetSmile = nn.L1Loss()
    runLossNetSmile = 0
    nbrErr = 0

    optiPosPts = optim.SGD(netPosPts.parameters(), lr=0.00007, momentum = 0.9)
    critPosPts = nn.L1Loss()
    runLossPosPts = 0

    nbrImage = len(os.listdir(imgFolder)) / 2


    for i in range(1, 1000):
        # Prem : 0.0015/i
        #optiPosPts = optim.SGD(netPosPts.parameters(), lr=0.00001, momentum = 0.9)
        id = 1
        while osp.exists(imgFolder + str(id) + 'a.jpg') :

            # Ouverture des images
            frmA  = Image.open(imgFolder+ str(id) + 'a.jpg').convert('RGB')
            frmB  = Image.open(imgFolder+ str(id) + 'b.jpg').convert('RGB')

            # Rotation aélatoire pour compliqué l'entrainement
            rot = rdm.randint(-5,5)
            frmA = frmA.rotate(rot)
            frmB = frmB.rotate(rot)


            # Conversion en PIL
            frmA = np.array(frmA)
            frmA = frmA[:, :, ::-1].copy()

            frmB = np.array(frmB)
            frmB = frmB[:, :, ::-1].copy()


            # Entrainement détection sourire
            for (frm, label) in [(frmA,[0,1]) , (frmB, [1,0])]:
                img = TraitementImage(frm)
                optiNetSmile.zero_grad()
                output = netSmile(img.unsqueeze(0)).flatten()
                loss = critNetSmile(output, torch.Tensor(label))
                loss.backward()
                optiNetSmile.step()

                if label == [0,1]:
                    output1 = output
                if label == [1,0]:
                    output2 = output

                if(label[1] != output.argmax()) :
                    nbrErr +=1
                runLossNetSmile += loss.item()



            # Entrainement Modification des points
            pts = PtsVisage(frmA)
            optiPosPts.zero_grad()
            output4 = netPosPts(pts.unsqueeze(0))
            resl_at = PtsVisage(frmB).flatten()
            resl_at = resl_at.type(torch.FloatTensor)
            loss = critPosPts(output4,resl_at)
            loss.backward()
            optiPosPts.step()
            runLossPosPts += loss.item()
            if (id + (i-1) *nbrImage) % 20  == 0.0:
                torch.save(netSmile, './Net/net_Smile_record.pt')
                torch.save(netPosPts, './Net/net_position_Pts_record.pt')
                print('Entrainement : {}/{} ({:.0f}%)'.format(
                    id + (i-1) *nbrImage, nbrImage*1000,
                    100. * (id + i *nbrImage) / (nbrImage*1000)))
                print(" Dernier test pour la detection de sourire :")
                print("  Resultat attendu : [0,1] et [1,0]")
                print("  Resultat obtenu : {} et {}".format(output1, output2))
                print(" {} erreurs".format(nbrErr))
                print(" \n Loss détection de sourire : {:.6f}".format(runLossNetSmile))
                runLossNetSmile = 0
                nbrErr = 0
                print(" Dernier test pour la création de sourire :")
                print("  Resultat attendu : ",resl_at)
                print("  Resultat obtenu : {}".format(output4))
                print(" \n Loss modification de points : {:.6f} \n".format(runLossPosPts))
                runLossPosPts = 0
            id += 1


    print("Stop training")

def premEntrainementNetSmile(net):
    print("Start training")

    # Chargement images + fichier labels
    labels = pd.read_csv('./data/Entrainement_Emotion/list_attributs.csv', delimiter=',')
    imgFolder = './data/Entrainement_Emotion/data/'

    net.train()
    optimizer = optim.SGD(net.parameters(), lr=0.0003)
    criterion = nn.L1Loss()

    running_loss = 0
    nbrErr = 0


    for id, nmFile in enumerate(labels['image_id']):
        try:
            frm  = cv2.imread(imgFolder+nmFile, 1)
            gray = cv2.cvtColor(frm, cv2.COLOR_BGR2GRAY)


            # Détection du visage
            rects = detector(gray, 0)
            i = 0

            # Pour chaque tete détecté
            for rect in rects:
                shape = predictor(gray, rect)
                shape = face_utils.shape_to_np(shape)

                i = i + 1
                if(rect.left()-20 > 0) :
                    left = rect.left()-10
                else :
                    left = 0
                if(rect.top()-20 > 0) :
                    top = rect.top()-10
                else:
                    top = 0

                right = rect.right()+20
                bottom = rect.bottom()+20
                rgn_face = frm[top:bottom, left:right]
                rgn_face = Image.fromarray(rgn_face)
                img = rgn_face.resize((160,260))


            optimizer.zero_grad()
            # Entrainement avec l'image
            img = Net.transf(img).to(device)
            output = net(img.unsqueeze(0)).flatten()

            reslt_att =  Net.F.relu(torch.Tensor([
                                    labels['Smiling'][id], -1*labels['Smiling'][id]]))
            loss = criterion(output,reslt_att)
            loss.backward()
            optimizer.step()

            if(Net.F.relu(torch.Tensor(
                    [labels['Smiling'][id], -1*labels['Smiling'][id]]))
                    .argmax() !=
                    output.argmax()) :
                nbrErr +=1

            running_loss += loss.item()
            if id % 50 == 0:
                torch.save(net, './Net/net_Smile_record.pt')
                print('Entrainement : {}/{} ({:.0f}%) \t Loss: {:.6f}'.format(
                    id, len(labels),
                    100. * id / len(labels), running_loss))
                print(" Dernier test :")
                print( "Resultat attendu :", reslt_att)
                print("  Resultat obtenu : {}".format(output))
                print(" {} erreurs".format(nbrErr))
                running_loss = 0
                nbrErr = 0
        except:
            pass



    print("Stop training")

def training() :
        netSmile = Net.ImportNetSmile()
        netPosPts = Net.ImportNetPositionPts()
        print(netSmile)
        print(netPosPts)

        if osp.exists("./Net/net_Smile.pt") == False:
            premEntrainementNetSmile(netSmile)
            torch.save(netSmile, './Net/net_Smile_first.pt')

        TrainingNet(netSmile, netPosPts)
        torch.save(netSmile, './Net/net_Smile.pt')
        torch.save(netPosPts, './Net/net_position_Pts.pt')

        if osp.exists("./Net/net_Smile_record.pt") == True:
            os.remove("./Net/net_Smile_record.pt")
        if osp.exists("./Net/net_position_Pts_record.pt") == True:
            os.remove("./Net/net_position_Pts_record.pt")

        print("Sauvegarde effectué!")



if __name__ == "__main__":
    training()