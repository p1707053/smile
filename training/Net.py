
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms

import os.path as osp


transf = transforms.Compose([
    transforms.RandomAffine((-10,10)),
    transforms.ToTensor(),
    transforms.Normalize((0.5,),(0.5,))
])

transfCreateSmile = transforms.Compose([
    transforms.ToTensor()
])


class NetSmile(nn.Module):
    def __init__(self):
        super(NetSmile, self).__init__()

        self.conv1 = nn.Conv2d(3,16,5)
        self.conv2 = nn.Conv2d(16,32,5)


        self.fc1 = nn.Linear(73408,2048)
        self.fc2 = nn.Linear(2048,1024)
        self.fc3 = nn.Linear(1024,518)
        self.fc4 = nn.Linear(518,256)
        self.fc5 = nn.Linear(256,2)


    def forward(self, x):

        x = self.conv1(x)
        x = F.max_pool2d(x, 2)
        x = self.conv2(x)
        x = F.max_pool2d(x, 2)

        x = torch.flatten(x, 1)

        x = nn.Softsign()(self.fc1(x))
        x = nn.Softsign()(self.fc2(x))
        x = nn.Softsign()(self.fc3(x))
        x = nn.Softsign()(self.fc4(x))
        x = nn.Softsign()(self.fc5(x))

        return x


class NetPositionPts(nn.Module):
    def __init__(self):
        super(NetPositionPts, self).__init__()

        self.fc1 = nn.Linear(136,256)
        self.fc2 = nn.Linear(256,512)
        self.fc3 = nn.Linear(512,1024)
        self.fc4 = nn.Linear(1024,1024)
        self.fc5 = nn.Linear(1024,512)
        self.fc6 = nn.Linear(512,256)
        self.fc7 = nn.Linear(256,136)

    def forward(self, x):
        x = x.type(torch.FloatTensor)

        x = nn.PReLU()(self.fc1(x.flatten()))
        x = nn.PReLU()(self.fc2(x.flatten()))
        x = nn.PReLU()(self.fc3(x.flatten()))
        x = nn.PReLU()(self.fc4(x.flatten()))
        x = nn.PReLU()(self.fc5(x.flatten()))
        x = nn.PReLU()(self.fc6(x.flatten()))
        x = nn.PReLU()(self.fc7(x.flatten()))

        #x = x.type(torch.IntTensor)
        return x


def ImportNetSmile(device ='cpu'):
    if osp.exists("./Net/net_Smile.pt") == True:
        netSmile = torch.load("./Net/net_Smile.pt")
    elif osp.exists("./training/Net/net_Smile.pt") == True:
        netSmile = torch.load("./training/Net/net_Smile.pt")
    else:
        netSmile = NetSmile().to(device)


    return netSmile


def ImportNetPositionPts(device ='cpu'):
    if osp.exists("./Net/net_position_Pts.pt") == True:
        netPosPts = torch.load("./Net/net_position_Pts.pt")
    elif osp.exists("./training/Net/net_position_Pts.pt") == True:
        netPosPts = torch.load("./training/Net/net_position_Pts.pt")
    else:
        netPosPts = NetPositionPts().to(device)

    return netPosPts