from imutils.video import VideoStream
from imutils import face_utils
import numpy as np

import argparse

import imutils
import dlib
import cv2

from PIL import Image
from PIL import ImageTk

import src.Interface as interface
import src.Warping as warping
import src.OrientationVisage as OV
import sys
sys.path.insert(0, './training')

from training import Net


#initialisation :
print("Initialisation...")

ap = argparse.ArgumentParser(description='Smile')
ap.add_argument('-W', '--width', default = 400, metavar = 'width', help='Largeur de l\'image (default : 400)')
ap.add_argument('-H', '--height', default = 400, metavar = 'height',
                    help='Hauteur de l\'image (default : 400)')
ap.add_argument('-d', '--device', default = "cpu", choices = ['cpu', 'cuda'],
                    help='Quel technologie utiliser', metavar = 'device')
ap.add_argument('--debug', dest='debug', action='store_true')
ap.set_defaults(feature=False)

agrs = ap.parse_args()


HEIGHT = agrs.height
WIDTH = agrs.width
device = Net.torch.device(agrs.device)
debug = agrs.debug

# Initialisation detection visage
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('./training/Net/face_detector.dat')


print("Chargement des système de neuronne...")
netSmileByImage =  Net.ImportNetSmile()
netChangePt = Net.ImportNetPositionPts()

print("Chargement interface...")
root = interface.tk.Tk()
root.wm_title("Smile")
wdw = interface.Interface(root)

vs = VideoStream().start()

ov = OV.OrientationVisage()


def ChangeImage(image, rects, ptsOrig):
    # Changement de base : Image to rectangle
    pts = ptsOrig- [rects[0][0], rects[1][0]]

    # On aligne le visage
    ov.ChangePts(pts)
    ov.TrouverTransl()
    pts = ov.ApplicationTransl(pts)


    # Application du système de réseaux
    ptInNet = Net.torch.from_numpy(pts)
    newPts = netChangePt(ptInNet)

    # Transformation en matrice d'entier
    newPts = newPts.resize(int(newPts.size()[0]/2), 2)
    newPts = newPts.detach().numpy()
    newPts = newPts.astype(int)

    # On Remet les points à leur place
    newPts = ov.ApplicationTransl(newPts, True)
    pts = ov.ApplicationTransl(pts, True)

    # Replace les points => le menton sur le menton
    x = pts[8][0] - newPts[8][0]
    y = pts[8][1] - newPts[8][1]
    newPts = newPts + [x,y]

    # Point d'ancrage -> Point inchange lors du warping :
    # certain point du rectangle
    r = rects[0][1]-rects[0][0]
    b = rects[1][1]-rects[1][0]

    ptInc = np.array([[0,0],
                      [int(r/2), 0],
                      [0, int(b/2)],
                      [r, 0]])
    pts = np.concatenate((ptInc, pts))
    newPts = np.concatenate((ptInc, newPts))


    ptInc = np.array([[0,b],
                      [int(r/2), b],
                      [r, int(b/2)],
                      [r, b]])
    pts = np.concatenate((ptInc, pts))
    newPts = np.concatenate((ptInc, newPts))


    # On applique le warping
    wpg = warping.Warping(image, pts, newPts)
    newImg = wpg.Execution()

    # On retourne dans le repère de l'image
    newPts = newPts + [rects[0][0], rects[1][0]]

    return (newImg, newPts)


def TraitementImage(frm):
    nbSmile, nbFace = 0, 0

    frm = imutils.resize(frm, width=WIDTH, height = HEIGHT)
    gray = cv2.cvtColor(frm, cv2.COLOR_BGR2GRAY)
    frm = cv2.cvtColor(frm, cv2.COLOR_BGR2RGB)


    # Détection du visage
    rects = detector(gray, 0)

    # Pour chaque tete détecté
    for rect in rects:
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        nbFace = nbFace + 1

        # Aggrandissement du rectangle + dessin
        if(rect.left()-25 > 0) :
            left = rect.left()-25
        else :
            left = 0
        if(rect.top()-5 > 0) :
            top = rect.top()-5
        else:
            top = 0

        if(rect.right()+25 <= frm.shape[1]) :
            right = rect.right()+25
        else :
            right = frm.shape[1]
        if(rect.bottom()-30 <= frm.shape[0]) :
            bottom = rect.bottom()+30
        else:
            bottom = frm.shape[0]



        # Traitement pour savoir si la personne souris
        # Resultat [oui, non]
        face = frm[top:bottom, left:right]
        face = Image.fromarray(face)
        rgn_face = face.resize((160,260))
        rgn_face = Net.transf(rgn_face).to(device)

        rslt = netSmileByImage(rgn_face.unsqueeze(0)).flatten()
        rslt = rslt - Net.torch.Tensor([0.1,0])
        if debug :
            print(rslt)
        rslt = rslt.argmax()

        if(rslt.item() == 0):
            nbSmile=nbSmile+1



        # Creation du sourir si pas de sourir
        if wdw.modifieFace.get() == 1 and rslt.item()!= 0:
            (newfrm, shape) = ChangeImage(frm[top:bottom, left:right], [[left, right],[top,bottom]], shape)
            try:
                for i in range(left, right):
                    for j in range(top, bottom):
                        frm[j][i]= newfrm[j-top][i-left]
            except:
                pass


        # Dessine le rectangle ou les points si besoin
        if wdw.afficherPtVisage.get() == 1 :
            for (x, y) in shape:
                cv2.circle(frm, (x, y), 1, (0, 0, 255), -1)
            if debug :
                ov.ChangePts(shape)
                ov.TrouverTransl()
                tempshape = ov.ApplicationTransl(shape)
                for (x, y) in tempshape:
                    cv2.circle(frm, (x, y), 1, (0, 255, 0), -1)

        #print(rect)
        if wdw.afficherRectangle.get() == 1 :
            cv2.rectangle(frm, (left, top),
                              (right, bottom),
                              (0,0,255), 2)

    if wdw.mirror.get() == 1 :
        frm = cv2.flip(frm, 1)
    # retourne : image, nombre de visage, combien sourit
    return (frm, nbFace, nbSmile)


def main():
    global frame
    while True:
       try:
           if not wdw.pause :
               # Lecture de la webcam
               frame = vs.read()

           (frm, nbFace, nbSmile) = TraitementImage(frame)


           # show the frame
           #frm = cv2.cvtColor(frm, cv2.COLOR_BGR2RGB)
           frm = Image.fromarray(frm)
           frm = ImageTk.PhotoImage(frm)


           wdw.cvsImage.imgtk = frm
           wdw.cvsImage.configure(image=frm)

           wdw.txtFace.set("Nombre de personne détecté : "+str(nbFace))
           wdw.txtSmile.set("Nombre de sourire détecté : "+str(nbSmile))
           wdw.update()

           global WIDTH, HEIGHT
           (WIDTH,HEIGHT) = wdw.DimensionImage()
       except:
           break

def ext():
    cv2.destroyAllWindows()
    vs.stop()

if __name__ == "__main__":
    main()
    ext()