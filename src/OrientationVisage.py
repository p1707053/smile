
import math
import numpy as np
import cv2

class OrientationVisage():

    def ChangePts(self, shp):
        self.vectImgToVis = np.array(shp[33])
        shape = shp
        shape = [-self.vectImgToVis[0], self.vectImgToVis[1]] + [1,-1] * shape
        self.ptsVis = np.array([
                                        (0, 0),     # Nez
                                        (shape[8][0], shape[8][1]),     # Menton
                                        (shape[36][0], shape[36][1]),     # Coin Yeux Droit
                                        (shape[45][0], shape[45][1]),     # Coin Yeux Gauche
                                        (shape[48][0], shape[48][1]),     # Coin Gauche Bouche
                                        (shape[54][0], shape[54][1])      # Coin droite Bouche
                                    ], dtype="double")


    def TrouverTransl(self):
        # Translation si le visage penche a gauche ou a droite (Rotation Z)
        a = abs(self.ptsVis[2][0]-self.ptsVis[3][0])
        o = abs(self.ptsVis[2][1]-self.ptsVis[3][1])
        h = math.sqrt(math.pow(a, 2)+ math.pow(o, 2))

        self.translZ = [[a/h, -(o/h)],
                    [o/h, a/h]]

        if(self.ptsVis[1][0] > 0):
            self.translZ = [[a/h, (o/h)],
                        [-o/h, a/h]]


        # Translation si le visage tourne a gauche ou a droite (Rotation Y)
        '''
        newPtsVis = self.ptsVis
        newPtsVis[2] = np.dot(self.translZ,newPtsVis[2])
        newPtsVis[3] = np.dot(self.translZ,newPtsVis[3])
        d1 = abs(newPtsVis[2][0])
        d2 = abs(newPtsVis[3][0])
        if d1 > d2 :
            self.translY = [[(d2)/d1, 0],
                        [0, 1]]
        else:
            self.translY = [[(d1)/d2, 0],
                        [0, 1]]

        '''
        self.translY = [[1,0],[0,1]]


    def ApplicationTransl(self, shp, Inverse =  False):
        shape = [-self.vectImgToVis[0], self.vectImgToVis[1]] + [1,-1] * shp

        if Inverse :
            for (i, pts) in enumerate(shape) :
                shape[i] = np.dot(np.linalg.inv(self.translZ),np.dot(np.linalg.inv(self.translY)  , pts))
        else:
            for (i, pts) in enumerate(shape) :
                shape[i] = np.dot(self.translY, np.dot( self.translZ, pts))

        shape = [self.vectImgToVis[0], self.vectImgToVis[1]] + [1,-1] * shape

        return shape

    def PtVectNormVis(self, size):

        pts = self.ptsVis

        coef = (self.ptsVis[1][1])/(-330)
        model_points = np.array([
                                (0.0, 0.0, 0.0),             # Nez
                                (0.0, -330.0*coef, -65.0*coef),        # Menton
                                (-225.0*coef, 170.0*coef, -135.0*coef),     # Coin Yeux Droit
                                (225.0*coef, 170.0*coef, -135.0*coef),      # Coin Yeux Gauche
                                (-150.0*coef, -150.0*coef, -125.0*coef),    # Coin Gauche Bouche
                                (150.0*coef, -150.0*coef, -125.0*coef)      # Coin droite Bouche

                                ])
        print(model_points)

        focal_length = size[1]
        center = (size[1]/2, size[0]/2)
        camera_matrix = np.array(
                                 [[focal_length, 0, center[0]],
                                 [0, focal_length, center[1]],
                                 [0, 0, 1]], dtype = "double"
                                 )

        dist_coeffs = np.zeros((4,1)) # Assuming no lens distortion
        (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, pts, camera_matrix, dist_coeffs, flags= cv2.SOLVEPNP_ITERATIVE)

        (nose_end_point2D, jacobian) = cv2.projectPoints(np.array([(0.0, 0.0, 1000.0)]), rotation_vector, translation_vector, camera_matrix, dist_coeffs)

        p1 = ( int(pts[0][0]), int(pts[0][1]))
        p1 =(self.vectImgToVis[0] + p1[0], self.vectImgToVis[1]-p1[1])

        p2 = ( int(nose_end_point2D[0][0][0]), int(nose_end_point2D[0][0][1]))
        p2 =(self.vectImgToVis[0] + p2[0], self.vectImgToVis[1]-p2[1])

        return (p1, p2)