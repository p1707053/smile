import numpy as np
from skimage import data
from skimage.viewer import ImageViewer

import skimage
import skimage.transform

from scipy.interpolate import Rbf


####Warping by RBF
class PointsRBF:
    def __init__(self, src, dst):
        xsrc = src[:,0]
        ysrc = src[:,1]
        xdst = dst[:,0]
        ydst = dst[:,1]
        # doc for RBF: https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.Rbf.html
        self.rbf_x = Rbf( xsrc, ysrc, xdst, function='linear')
        self.rbf_y = Rbf( xsrc, ysrc, ydst, function='linear')


    def __call__(self, xy):
        x = xy[:,0]
        y = xy[:,1]
        xdst = self.rbf_x(x,y)
        ydst = self.rbf_y(x,y)
        return np.transpose( [xdst,ydst] )

class Warping():

    def __init__(self, image, pts, newPts):
        self.pts = pts
        self.newPts = newPts
        self.img = image

    def Execution(self):
        try:
            # traiter les points
            prbf = PointsRBF( self.newPts, self.pts)
            # warper
            warped = skimage.transform.warp(self.img, prbf)
            # Mettre en couleur et convertion
            warped *= 255
            warped = warped.astype(np.uint8)
            return warped
        except:
            return self.image


