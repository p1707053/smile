import tkinter as tk
import tkinter.ttk as ttk

from PIL import ImageTk

import os as os
import os.path as osp

formatImage = [
        ["480x320", 480, 320],
        ["800x480", 800, 480],
        ["1024x576", 1024, 576],
        ["1280x720", 1280, 700]]
formatImageTxt = ["480x320", "800x480", "1024x576", "1280x720"]



class Interface(tk.Frame):

    def __init__(self, window, **kwargs):
        tk.Frame.__init__(self, window, **kwargs)

        self.pause = False

        # Cadre caméra
        cadre1 = tk.LabelFrame(self, text = "Caméra")
        cadre1.pack(fill=tk.X, padx=10, pady = 10, ipadx = 10, ipady = 10 )
        self.cvsImage = tk.Label(cadre1)
        self.cvsImage.pack()

        # Cadre information
        cadre2 = tk.LabelFrame(self, borderwidth = 1, relief = tk.GROOVE)
        cadre2.pack(fill=tk.X, padx = 10, pady = 10 )


        cadDim = tk.LabelFrame(cadre2, borderwidth = 1, relief = tk.GROOVE)
        cadDim.pack(fill=tk.X, padx=10, pady = 10)
        cadDim.columnconfigure(0, weight=3)
        cadDim.columnconfigure(1, weight=1)
        cadDim.columnconfigure(2, weight=3)
        cadDim.columnconfigure(3, weight=1)

        self.afficherRectangle = tk.IntVar()
        self.afficherRectangle.set(1)
        checkBtn1 = tk.Checkbutton(cadre2, text = "Afficher la détection du visage", variable = self.afficherRectangle)
        checkBtn1.pack()
        self.afficherPtVisage = tk.IntVar()
        self.afficherPtVisage.set(1)
        checkBtn2 = tk.Checkbutton(cadre2, text = "Afficher les points du visage", variable = self.afficherPtVisage)
        checkBtn2.pack()
        self.mirror = tk.IntVar()
        checkBtn3 = tk.Checkbutton(cadre2, text = "Afficher en miroir", variable = self.mirror)
        checkBtn3.pack()
        self.modifieFace = tk.IntVar()
        self.modifieFace.set(1)
        checkBtn4 = tk.Checkbutton(cadre2, text = "Modifier Visage", variable = self.modifieFace)
        checkBtn4.pack()


        lb2 = tk.Label(cadDim, text="Définition retour caméra : ")
        lb2.grid(column=0, row=0)
        self.lstDef = ttk.Combobox(cadDim, values=formatImageTxt)
        self.lstDef.grid(column=2, row=0)
        self.lstDef.current(0)

        self.txtFace = tk.StringVar()
        self.txtFace.set("Nombre de personne détecté : 0")
        self.txtSmile = tk.StringVar()
        self.txtSmile.set("Nombre de sourire détecté : 0")
        lab2 = tk.Label(cadre2, textvariable=self.txtFace)
        lab2.pack()
        lab3 = tk.Label(cadre2, textvariable=self.txtSmile)
        lab3.pack()

        # Gestion bouton
        cadre3 = tk.LabelFrame(self, borderwidth = 1, relief = tk.GROOVE, padx=10, pady = 10)
        cadre3.columnconfigure(0, weight=3)
        cadre3.columnconfigure(1, weight=1)
        cadre3.columnconfigure(2, weight=3)
        cadre3.columnconfigure(3, weight=1)
        cadre3.columnconfigure(4, weight=3)

        btn1 = tk.Button(cadre3, padx=10, pady = 10, text="Pause", command= self.Pause)
        btn1.grid(column=0, row=0)

        btn2 = tk.Button(cadre3, padx=10, pady = 10, text="Sauvegarder", command=self.Sauvegarde)
        btn2.grid(column=2, row=0)

        btn3 = tk.Button(cadre3, padx=10, pady = 10, text="Quit", command=(lambda : self.destroy()))
        btn3.grid(column=4, row=0)

        cadre3.pack(fill=tk.X, padx=10, pady = 10 )

        # Gestion keyboard
        self.bind_all("<Any-KeyPress>", lambda event : self.Pause(tp='sortie'))
        self.bind_all("<KeyPress-p>", self.PauseBtn)
        self.bind_all("<KeyPress-s>", self.SauvegardeBtn)
        self.bind_all("<Escape>", self.EchapBtn)


        self.pack()

        self.capture = False

    def PauseBtn(self, event, tp = 'inverse'):
        self.Pause(tp = tp)

    def Pause(self, tp = 'inverse'):
        if (tp == 'inverse'):
            self.pause = not self.pause
        elif (tp == 'sortie'):
            self.pause = False

    def DimensionImage(self):
        i = self.lstDef.current()
        return formatImage[i][1], formatImage[i][2]

    def EchapBtn(self, event):
        if( self.lstDef.current() == 0) :
            self.destroy()
        else :
            self.lstDef.current(0)

    def SauvegardeBtn(self, event):
        self.Sauvegarde()

    def Sauvegarde(self):
        img = ImageTk.getimage(self.cvsImage.imgtk)
        i = 0
        if osp.isdir("./screen") == False:
            os.mkdir("./screen")

        while osp.exists("./screen/"+str(i)+".png"):
            i += 1
        img.save("./screen/"+str(i)+".png")